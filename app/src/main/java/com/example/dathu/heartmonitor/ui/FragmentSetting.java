package com.example.dathu.heartmonitor.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.dathu.heartmonitor.HeartBeat;
import com.example.dathu.heartmonitor.LoginActivity;
import com.example.dathu.heartmonitor.MainApplication;
import com.example.dathu.heartmonitor.R;
import com.example.dathu.heartmonitor.ui.adapter.model.SettingItem;
import com.example.dathu.heartmonitor.ui.adapter.SettingMenuAdapter;

import java.io.IOException;
import java.util.ArrayList;

public class FragmentSetting extends Fragment{
    private RecyclerView rcvMenu;
    private SettingMenuAdapter adapter;
    private ArrayList<SettingItem> listSetting;
    private FragmentBluetooth frBluetooth;

    public static FragmentSetting newInstance(){
        return new FragmentSetting();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting, container, false);
        rcvMenu = v.findViewById(R.id.rcv_menu);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initListener();
    }

    private void initData(){
        listSetting = new ArrayList<>();
        listSetting.add(new SettingItem(R.drawable.setting, "Send data to Server", 0));
        listSetting.add(new SettingItem(R.drawable.setting, "Emergency notification", 0));
        listSetting.add(new SettingItem(R.drawable.bluetooth, "Bluetooth", 0));
        listSetting.add(new SettingItem(R.drawable.logout, "Log out", 0));
        adapter = new SettingMenuAdapter(getContext(), listSetting);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rcvMenu.setLayoutManager(layoutManager);
        rcvMenu.setAdapter(adapter);
    }

    private void initListener(){
        adapter.setOnItemClickListener(new SettingMenuAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                switch (position){
                    case 0:
                        if (listSetting.get(position).getStatus() != 1) {
//                            MainApplication.getInstance().setSendDataSetting(true);
                            adapter.setItemStatus(position, 1);
                        } else {
                            MainApplication.getInstance().setSendDataSetting(false);
                            adapter.setItemStatus(position, 0);
                        }
                        break;
                    case 1:
                        if (listSetting.get(position).getStatus() != 1) {
                            MainApplication.getInstance().setEmergencySetting(true);
                            adapter.setItemStatus(position, 1);
                        } else {
                            MainApplication.getInstance().setEmergencySetting(false);
                            adapter.setItemStatus(position, 0);
                        }
                        break;
                    case 2:
                        if (listSetting.get(position).getStatus()!=1){
                            frBluetooth = FragmentBluetooth.newInstance();
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.main_content,frBluetooth)
                                    .commit();
                            adapter.setItemStatus(position, 1);
                        } else {
//                            MainApplication.getInstance().getBluetoothService().cancel();
//                            try {
//                                MainApplication.getInstance().getBluetoothSocket().close();
//                            } catch (IOException e) {
//                                Log.e("Error", e.getMessage());
//                            }
//                            adapter.setItemStatus(position, 0);
                        }
                       break;
                    case 3:
                        Intent logoutIntent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(logoutIntent);
                        getActivity().finish();
//                        showToast("log out");
                }
            }
        });
    }

}
