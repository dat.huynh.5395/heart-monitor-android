package com.example.dathu.heartmonitor.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.dathu.heartmonitor.MainApplication;
import com.example.dathu.heartmonitor.R;
import com.example.dathu.heartmonitor.User;

public class FragmentProfile extends Fragment implements View.OnClickListener{
    private ImageButton btnEditUserName, btnEditGender, btnEditDob, btnEditHeight, btnEditWeight, btnEditPhone;
    private TextView txtName, txtUserName, txtGender, txtDoB, txtHeight, txtWeight, txtPhone;

    public static FragmentProfile newInstance(){
        return new FragmentProfile();
    }
    private User user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        btnEditUserName = v.findViewById(R.id.btn_edit_username);
        btnEditDob = v.findViewById(R.id.btn_edit_dob);
        btnEditGender = v.findViewById(R.id.btn_edit_gender);
        btnEditHeight = v.findViewById(R.id.btn_edit_height);
        btnEditWeight = v.findViewById(R.id.btn_edit_weight);
        btnEditPhone = v.findViewById(R.id.btn_edit_number);
        txtName = v.findViewById(R.id.txt_name);
        txtUserName = v.findViewById(R.id.txt_user_name);
        txtGender = v.findViewById(R.id.txt_gender);
        txtDoB = v.findViewById(R.id.txt_dob);
        txtHeight = v.findViewById(R.id.txt_height);
        txtWeight = v.findViewById(R.id.txt_weight);
        txtPhone = v.findViewById(R.id.txt_number);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initListener();
    }

    private void initData(){
        user = MainApplication.getInstance().getCurrentUser();
        if(user!=null){
            txtName.setText(user.name);
            txtUserName.setText(user.userName);
            txtGender.setText(user.isMale?"Male":"Female");
            txtDoB.setText(user.dob);
            txtHeight.setText(String.valueOf(user.height));
            txtWeight.setText(String.valueOf(user.weight));
            txtPhone.setText(String.valueOf(user.phone));
        }
    }

    private void initListener(){
        btnEditUserName.setOnClickListener(this);
        btnEditDob.setOnClickListener(this);
        btnEditGender.setOnClickListener(this);
        btnEditHeight.setOnClickListener(this);
        btnEditWeight.setOnClickListener(this);
        btnEditPhone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(user!=null){
            switch (v.getId()){
                case R.id.btn_edit_username:
                    new EditDialog(getActivity())
                            .setText(user.userName)
                            .show(new EditDialog.OnClickListener() {
                                @Override
                                public void onClick(String message) {
                                    txtUserName.setText(message);
                                }
                            });
                    break;
                case R.id.btn_edit_gender:
                    new EditDialog(getActivity())
                            .setText(user.isMale?"Male":"Female")
                            .show(new EditDialog.OnClickListener() {
                                @Override
                                public void onClick(String message) {
                                    txtGender.setText(message);
                                }
                            });
                    break;
                case R.id.btn_edit_dob:
                    new EditDialog(getActivity())
                            .setText(user.dob)
                            .show(new EditDialog.OnClickListener() {
                                @Override
                                public void onClick(String message) {
                                    txtDoB.setText(message);
                                }
                            });
                    break;
                case R.id.btn_edit_height:
                    new EditDialog(getActivity())
                            .setText(String.valueOf(user.height))
                            .show(new EditDialog.OnClickListener() {
                                @Override
                                public void onClick(String message) {
                                    txtHeight.setText(message);
                                }
                            });
                    break;
                case R.id.btn_edit_weight:
                    new EditDialog(getActivity())
                            .setText(String.valueOf(user.weight))
                            .show(new EditDialog.OnClickListener() {
                                @Override
                                public void onClick(String message) {
                                    txtWeight.setText(message);
                                }
                            });
                    break;
                case R.id.btn_edit_number:
                    new EditDialog(getActivity())
                            .setText(user.phone)
                            .show(new EditDialog.OnClickListener() {
                                @Override
                                public void onClick(String message) {
                                    txtPhone.setText(message);
                                }
                            });
                    break;
            }
        }
    }
}
