package com.example.dathu.heartmonitor;

import android.app.Activity;
import android.util.Log;

public class Util {
    private static final String TAG = Util.class.getSimpleName();
    private static boolean isLog = true;

    public static void log(String logMessage){
        if(isLog){
            Log.e(
                    TAG, new Throwable().getStackTrace()[1].toString()
                    +"\n"
                    +"Message: "+logMessage
            );
        }
    }

    public static void apiLog(String logMessage){
        if(isLog){
            Log.i(
                    TAG, new Throwable().getStackTrace()[1].toString()
                    +"\n"
                    +"Message: "+logMessage
            );
        }
    }


}
